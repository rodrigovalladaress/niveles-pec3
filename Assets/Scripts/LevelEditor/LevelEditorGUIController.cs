﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class LevelEditorGUIController : LevelEditorBehaviour {

    [SerializeField]
    private GameObject editorModeGUI;
    [SerializeField]
    private GameObject testEditorModeGUI;
    [SerializeField]
    private Text levelNameGUI;

    private new void Awake() {
        base.Awake();
        UpdateLevelName(Level.DEFAULT_NAME);
    }

    private void OnEnable() {
        ThisMaster.gameModeSetted += OnGameModeSetted;
        ThisMaster.levelLoaded += OnLevelLoaded;
        ThisMaster.saveLevelSuccess += OnSaveLevelSuccess;
    }

    private void OnDisable() {
        ThisMaster.gameModeSetted -= OnGameModeSetted;
        ThisMaster.levelLoaded -= OnLevelLoaded;
        ThisMaster.saveLevelSuccess -= OnSaveLevelSuccess;
    }

    private void OnGameModeSetted(Master.GameMode gameMode) {
        switch(gameMode) {
            case Master.GameMode.editor:
                editorModeGUI.SetActive(true);
                testEditorModeGUI.SetActive(false);
                break;
            case Master.GameMode.normalPlay:
                editorModeGUI.SetActive(false);
                testEditorModeGUI.SetActive(false);
                break;
            case Master.GameMode.testEditor:
                editorModeGUI.SetActive(false);
                testEditorModeGUI.SetActive(true);
                break;
        }
    }

    private void OnLevelLoaded(Level level) {
        UpdateLevelName(level.Name);
    }

    private void OnSaveLevelSuccess(Level level) {
        UpdateLevelName(level.Name);
    }

    private void UpdateLevelName(string name) {
        levelNameGUI.text = name;
    }
}
