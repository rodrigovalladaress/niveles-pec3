﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelEditorGUIElement : MonoBehaviour {

    [SerializeField]
    private FontStyle selectedFontStyle = FontStyle.Bold;

    private Text textComponent;
    private string originalText;
    private FontStyle unselectedFontStyle;

    private void Awake() {
        textComponent = GetComponent<Text>();
        originalText = textComponent.text;
        unselectedFontStyle = textComponent.fontStyle;
    }

    public void Select() {
        textComponent.text = "( " + originalText + " )";
        textComponent.fontStyle = selectedFontStyle;
    }

    public void UnSelect() {
        textComponent.text = originalText;
        textComponent.fontStyle = unselectedFontStyle;
    }

}
