﻿using System;
using UnityEngine;

public class LevelEditorMouseCollider : LevelEditorBehaviour {

    private LevelElementInfo info;
    
    private new void Awake() {
        base.Awake();
        info = GetComponent<LevelElementInfo>();
        
    }

    private void OnMouseOver() {
        MouseAction();
    }

    private void OnMouseDrag() {
        MouseAction();
    }

    private void MouseAction() {
        if (Master.EditorMode) {
            LevelElement level = info.LevelElement;

            foreach (Master.MouseButton button in
                Enum.GetValues(typeof(Master.MouseButton))) {
                if (Input.GetMouseButton((int)button) ||
                    Input.GetMouseButtonDown((int)button)) {
                    ThisMaster.CallLevelElementClicked(level, button);
                }
            }
        }
    }

}
