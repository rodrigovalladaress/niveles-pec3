﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEditorController : LevelEditorBehaviour {

    [SerializeField]
    private KeyCode boxKeyCode = KeyCode.Q;
    [SerializeField]
    private KeyCode wallKeyCode = KeyCode.W;
    [SerializeField]
    private KeyCode goalKeyCode = KeyCode.E;
    [SerializeField]
    private KeyCode startKeyCode = KeyCode.R;
    [SerializeField]
    private KeyCode saveKeyCode = KeyCode.S;
    [SerializeField]
    private KeyCode openKeyCode = KeyCode.K;
    [SerializeField]
    private KeyCode testLevelKeyCode = KeyCode.P;
    [SerializeField]
    private KeyCode cancelTestKeyCode = KeyCode.Q;
    [SerializeField]
    private KeyCode restartTestKeyCode = KeyCode.R;

    [SerializeField]
    private LevelEditorGUIElement boxGUI;
    [SerializeField]
    private LevelEditorGUIElement wallGUI;
    [SerializeField]
    private LevelEditorGUIElement goalGUI;
    [SerializeField]
    private LevelEditorGUIElement startGUI;

    private LevelEditorGUIElement GUIselected;

    private void OnEnable() {
        ThisMaster.levelFinished += OnLevelFinished;
    }

    private void OnDisable() {
        ThisMaster.levelFinished += OnLevelFinished;
    }

    private void Start() {
        SelectGUIElement(boxGUI, LevelEditor_Master.LevelEditorTool.box);
    }

    private void Update() {
        if (Master.EditorMode) {
            if (Input.GetKeyDown(boxKeyCode)) {
                UnselectGUIElement();
                SelectGUIElement(boxGUI,
                    LevelEditor_Master.LevelEditorTool.box);
            }
            if (Input.GetKeyDown(wallKeyCode)) {
                UnselectGUIElement();
                SelectGUIElement(wallGUI,
                    LevelEditor_Master.LevelEditorTool.wall);
            }
            if (Input.GetKeyDown(goalKeyCode)) {
                UnselectGUIElement();
                SelectGUIElement(goalGUI,
                    LevelEditor_Master.LevelEditorTool.goal);
            }
            if (Input.GetKeyDown(startKeyCode)) {
                UnselectGUIElement();
                SelectGUIElement(startGUI,
                    LevelEditor_Master.LevelEditorTool.start);
            }
            if(Input.GetKeyDown(testLevelKeyCode)) {
                ThisMaster.CallGameModeSetted(Master.GameMode.testEditor);
            }
            if(Input.GetKeyDown(openKeyCode)) {
                ThisMaster.CallOpenLevelRequested();
            }
            if(Input.GetKeyDown(saveKeyCode)) {
                ThisMaster.CallSaveLevelRequested();
            }
        } else if(Master.TestEditorMode) {
            if(Input.GetKeyDown(cancelTestKeyCode)) {
                ThisMaster.CallLevelRestarted();
                ThisMaster.CallGameModeSetted(Master.GameMode.editor);
            }
            if (Input.GetKeyDown(restartTestKeyCode)) {
                ThisMaster.CallLevelRestarted();
            }
        }
    }

    private void UnselectGUIElement() {
        if (GUIselected) {
            GUIselected.UnSelect();
        }
    }

    private void SelectGUIElement(LevelEditorGUIElement element,
        LevelEditor_Master.LevelEditorTool tool) {
        GUIselected = element;
        element.Select();
        ThisMaster.CallToolSelected(tool);
    }

    private void OnLevelFinished() {
        if(Master.TestEditorMode) {
            ThisMaster.CallLevelRestarted();
            ThisMaster.CallGameModeSetted(Master.GameMode.editor);
        }
    }
}
