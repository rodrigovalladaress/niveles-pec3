﻿using UnityEngine;

public class LevelEditorManager : LevelEditorBehaviour {

    private const Master.MouseButton ADD = Master.MouseButton.left;
    private const Master.MouseButton DELETE  = Master.MouseButton.right;
    private const Master.MouseButton MOVE = Master.MouseButton.middle;

    private LevelEditor_Master.LevelEditorTool toolSelected;

    private void OnEnable() {
        ThisMaster.levelElementClicked += OnLevelElementClicked;
        ThisMaster.toolSelected += OnToolSelected;
    }

    private void OnDisable() {
        ThisMaster.levelElementClicked -= OnLevelElementClicked;
        ThisMaster.toolSelected -= OnToolSelected;
    }

    private void OnLevelElementClicked(LevelElement element, 
        Master.MouseButton button) {
        if(Master.EditorMode) {
            IntVector2 position = element.Position;
            if(button == ADD) {
                // Debug.Log("add " + element);
                switch(toolSelected) {
                    case LevelEditor_Master.LevelEditorTool.box:
                        ThisMaster.CallTryToAddElement(new Box(position));
                        break;
                    case LevelEditor_Master.LevelEditorTool.wall:
                        ThisMaster.CallTryToAddElement(new Wall(position));
                        break;
                    case LevelEditor_Master.LevelEditorTool.goal:
                        ThisMaster.CallTryToAddElement(new Goal(position));
                        break;
                    case LevelEditor_Master.LevelEditorTool.start:
                        ThisMaster.CallTryToAddElement(
                            new PlayerStart(position));
                        break;
                }
            } else if(button == DELETE) {
                Debug.Log("delte");
                ThisMaster.CallLevelElementsDeletedIn(position);
            }
        }
    }

    private void OnToolSelected(LevelEditor_Master.LevelEditorTool tool) {
        toolSelected = tool;
    }

}
