﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorMouseOver : LevelEditorBehaviour {

    [SerializeField]
    private Color onMouseOverColor = Color.blue;

    private SpriteRenderer spriteRenderer;
    private Color originalColor;

    private new void Awake() {
        base.Awake();
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalColor = spriteRenderer.color;
    }

    private void OnMouseOver() {
        if(Master.EditorMode) {
            spriteRenderer.color = onMouseOverColor;
        }
    }

    private void OnMouseExit() {
        if (Master.EditorMode) {
            spriteRenderer.color = originalColor;
        }
    }

}
