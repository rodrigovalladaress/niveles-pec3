﻿public class LevelEditor_Master : Master {

    public enum LevelEditorTool {
        box,
        wall,
        goal,
        start
    }

    public delegate void LevelEditorToolHandler(LevelEditorTool tool);
    public event LevelEditorToolHandler toolSelected;

    public new void CallGameModeSetted(GameMode gameMode) {
        base.CallGameModeSetted(gameMode);
        globalMaster.CallGameModeSetted(gameMode);
    }

    public new void CallLevelRestarted() {
        base.CallLevelRestarted();
        globalMaster.CallLevelRestarted();
    }

    public new void CallOpenLevelRequested() {
        base.CallOpenLevelRequested();
        globalMaster.CallOpenLevelRequested();
    }

    public new void CallTryToAddElement(LevelElement element) {
        globalMaster.CallTryToAddElement(element);
    }

    public new void CallSaveLevelRequested() {
        globalMaster.CallSaveLevelRequested();
    }

    public new void CallLevelElementsDeletedIn(IntVector2 position) {
        globalMaster.CallLevelElementsDeletedIn(position);
    }

    public void CallToolSelected(LevelEditorTool tool) {
        if(toolSelected != null) {
            toolSelected(tool);
        }
    }

}
