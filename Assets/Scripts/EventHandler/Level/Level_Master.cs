﻿using UnityEngine;

public class Level_Master : Master {

    private Level level;

    public Level Level {
        get {
            return level;
        }

        set {
            level = value;
        }
    }

    public event LevelElementHandler levelElementSucessAdded;

    public new void CallLevelLoaded(Level level) {
        this.level = level;
        base.CallLevelLoaded(level);
        globalMaster.CallLevelLoaded(level);
    }

    public new void CallLevelDrawn() {
        base.CallLevelDrawn();
        globalMaster.CallLevelDrawn();
    }

    public new void CallLevelFinished() {
        base.CallLevelFinished();
        globalMaster.CallLevelFinished();
    }

    public new void CallSaveLevelSuccess(Level level) {
        base.CallSaveLevelSuccess(level);
        globalMaster.CallSaveLevelSuccess(level);
    }

    public new void CallGoalActivationDeltaChanged(int delta) {
        base.CallGoalActivationDeltaChanged(delta);
        globalMaster.CallGoalActivationDeltaChanged(delta);
    }

    public new void CallGameModeSetted(GameMode gameMode) {
        base.CallGameModeSetted(gameMode);
        // globalMaster.CallGameModeSetted(gameMode);
    }

    public new void CallLevelElementDrawn(LevelElement element,
        GameObject o) {
        base.CallLevelElementDrawn(element, o);
        globalMaster.CallLevelElementDrawn(element, o);
    }

    public void CallLevelElementSuccessAdded(LevelElement element) {
        if (levelElementSucessAdded != null) {
            levelElementSucessAdded(element);
        }
    }
}
