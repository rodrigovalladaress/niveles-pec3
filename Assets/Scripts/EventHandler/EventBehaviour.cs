﻿using UnityEngine;

public abstract class EventBehaviour : MonoBehaviour {

    private static int PARENT_LIMIT = 10;

    protected Master master;

    protected Master ThisMaster {
        get {
            return master;
        }
    }

    protected void Awake() {
        Initialize();
    }

    private void Initialize() {
        Transform from = transform;
        int checks = 0;
        do {
            master = from.GetComponent<Master>();
            from = from.parent;
            checks++;
        } while (from && !master && checks < PARENT_LIMIT);
        if (checks == PARENT_LIMIT) {
            Debug.LogError("Master of " + name + " couldn't be found.");
        }
    }

}
