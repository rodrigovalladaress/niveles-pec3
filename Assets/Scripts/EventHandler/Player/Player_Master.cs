﻿using UnityEngine;

public class Player_Master : Master {

    public new void CallTryToMovePlayer(IntVector2 direction) {
        base.CallTryToMovePlayer(direction);
        globalMaster.CallTryToMovePlayer(direction);
    }

}
