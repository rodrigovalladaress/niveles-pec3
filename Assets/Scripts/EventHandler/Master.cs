﻿using UnityEngine;

public class Master : MonoBehaviour {

    public enum MouseButton {
        left = 0,
        middle = 2,
        right = 1
    }

    public enum GameMode {
        none,
        editor,
        testEditor,
        normalPlay
    }

    protected static Global_Master globalMaster;
    private static GameMode currentGameMode;

    public static GameMode CurrentGameMode {
        get {
            return currentGameMode;
        }
    }

    public static bool EditorMode {
        get {
            return currentGameMode == GameMode.editor;
        }
    }

    public static bool TestEditorMode {
        get {
            return currentGameMode == GameMode.testEditor;
        }
    }

    public static bool NormalPlayMode {
        get {
            return currentGameMode == GameMode.normalPlay;
        }
    }

    public delegate void GameModeHandler(GameMode gameMode);
    public event GameModeHandler gameModeSetted;

    public delegate void GeneralEventHandler();
    public event GeneralEventHandler levelDrawn;
    public event GeneralEventHandler levelFinished;
    public event GeneralEventHandler openLevelRequested;
    public event GeneralEventHandler levelRestarted;
    public event GeneralEventHandler saveLevelRequested;

    public delegate void IntEventHandler(int _int);
    public event IntEventHandler goalActivationDeltaChanged;

    public delegate void BoolEventHandler(bool _bool);

    public delegate void LevelEventHandler(Level level);
    public event LevelEventHandler levelLoaded;
    public event LevelEventHandler saveLevelSuccess;

    public delegate void LevelElementHandler(LevelElement element);
    public event LevelElementHandler playerSpawned;
    public event LevelElementHandler tryToAddElement;
    public event LevelElementHandler levelElementDeleted;

    public delegate void LevelElementClickedHandler(LevelElement element,
        MouseButton button);
    public event LevelElementClickedHandler levelElementClicked;

    public delegate void LevelElementGameObjectHandler(LevelElement element,
        GameObject o);
    public event LevelElementGameObjectHandler levelElementDrawn;

    public delegate void IntVector2Handler(IntVector2 direction);
    public event IntVector2Handler tryToMovePlayer;
    public event IntVector2Handler playerMoved;
    public event IntVector2Handler levelElementsDeletedIn;

    public delegate void LevelElementMovementHandler(LevelElement element,
        IntVector2 from,
        IntVector2 to);
    public event LevelElementMovementHandler levelElementMoved;

    private void Awake() {
        if (globalMaster == null) {
            globalMaster = FindObjectOfType<Global_Master>();
        }
    }

    public void CallGameModeSetted(GameMode gameMode) {
        if (currentGameMode != gameMode) {
            currentGameMode = gameMode;
        }
        if (gameModeSetted != null) {
            gameModeSetted(gameMode);
        }
    }

    public void CallLevelDrawn() {
        if (levelDrawn != null) {
            levelDrawn();
        }
    }

    public void CallLevelFinished() {
        if (levelFinished != null) {
            levelFinished();
        }
    }

    public void CallOpenLevelRequested() {
        if(openLevelRequested != null) {
            openLevelRequested();
        }
    }

    public void CallLevelRestarted() {
        if(levelRestarted != null) {
            levelRestarted();
        }
    }

    public void CallSaveLevelRequested() {
        if (saveLevelRequested != null) {
            saveLevelRequested();
        }
    }

    public void CallSaveLevelSuccess(Level level) {
        if(saveLevelSuccess != null) {
            saveLevelSuccess(level);
        }
    }

    public void CallGoalActivationDeltaChanged(int delta) {
        if (goalActivationDeltaChanged != null) {
            goalActivationDeltaChanged(delta);
        }
    }

    public void CallLevelLoaded(Level level) {
        if (levelLoaded != null) {
            levelLoaded(level);
        }
    }

    public void CallPlayerSpawned(LevelElement element) {
        if (playerSpawned != null) {
            playerSpawned(element);
        }
    }

    public void CallTryToAddElement(LevelElement element) {
        if (tryToAddElement != null) {
            tryToAddElement(element);
        }
    }

    public void CallLevelElementDeleted(LevelElement element) {
        if (levelElementDeleted != null) {
            levelElementDeleted(element);
        }
    }

    public void CallLevelElementClicked(LevelElement element,
        MouseButton mouseButton) {
        if (levelElementClicked != null) {
            levelElementClicked(element, mouseButton);
        }
    }

    public void CallLevelElementDrawn(LevelElement element, GameObject o) {
        if (levelElementDrawn != null) {
            levelElementDrawn(element, o);
        }
    }

    public void CallTryToMovePlayer(IntVector2 direction) {
        if (tryToMovePlayer != null) {
            tryToMovePlayer(direction);
        }
    }

    public void CallPlayerMoved(IntVector2 target) {
        if (playerMoved != null) {
            playerMoved(target);
        }
    }

    public void CallLevelElementsDeletedIn(IntVector2 position) {
        if (levelElementsDeletedIn != null) {
            levelElementsDeletedIn(position);
        }
    }

    public void CallElementMoved(LevelElement element, IntVector2 from,
        IntVector2 to) {
        if (levelElementMoved != null) {
            levelElementMoved(element, from, to);
        }
    }

}
