﻿using System;
using UnityEngine;

public class Global_Master : Master { 
    [SerializeField]
    private Level_Master levelMaster;
    [SerializeField]
    private LevelEditor_Master levelEditorMaster;
    [SerializeField]
    private Player_Master playerMaster;

    public new void CallLevelFinished() {
        if(TestEditorMode) {
            Debug.Log("global master level editor finish level");
            levelEditorMaster.CallLevelFinished();
        }
    }

    public new void CallLevelDrawn() {
        playerMaster.CallLevelDrawn();
    }

    public new void CallOpenLevelRequested() {
        levelMaster.CallOpenLevelRequested();
    }

    public new void CallLevelRestarted() {
        levelMaster.CallLevelRestarted();
    }

    public new void CallSaveLevelSuccess(Level level) {
        if (EditorMode || TestEditorMode) {
            levelEditorMaster.CallSaveLevelSuccess(level);
        }
    }

    public new void CallGameModeSetted(GameMode gameMode) {
        playerMaster.CallGameModeSetted(gameMode);
        levelMaster.CallGameModeSetted(gameMode);
        // levelEditorMaster.CallGameModeSetted(gameMode);
    }

    public new void CallLevelLoaded(Level level) {
        if (EditorMode || TestEditorMode) {
            levelEditorMaster.CallLevelLoaded(level);
        }
    }

    public new void CallTryToAddElement(LevelElement element) {
        levelMaster.CallTryToAddElement(element);
    }

    public new void CallSaveLevelRequested() {
        levelMaster.CallSaveLevelRequested();
    }

    public new void CallLevelElementDrawn(LevelElement element,
        GameObject o) {
        if(EditorMode || TestEditorMode) {
            levelEditorMaster.CallLevelElementDrawn(element, o);
        }
    }

    public new void CallGoalActivationDeltaChanged(int delta) {
        // gameManagerMaster.CallGoalActivationDeltaChanged(delta);
    }

    public new void CallTryToMovePlayer(IntVector2 direction) {
        levelMaster.CallTryToMovePlayer(direction);
    }

    public new void CallPlayerMoved(IntVector2 target) {
        playerMaster.CallPlayerMoved(target);
    }

    public new void CallLevelElementsDeletedIn(IntVector2 position) {
        levelMaster.CallLevelElementsDeletedIn(position);
    }
}
