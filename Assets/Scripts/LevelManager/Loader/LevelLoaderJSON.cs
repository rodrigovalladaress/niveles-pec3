﻿using System;
using System.IO;
using UnityEngine;

public class LevelLoaderJSON : LevelLoader {
    protected override Level LoadInternal(string fullName) {
        Debug.Log("loading " + fullName);
        StreamReader reader = new StreamReader(fullName);

        string json = reader.ReadToEnd();
        return LoadContentInternal(fullName, json);
    }

    protected override Level LoadContentInternal(string name, object content) {
        string json = (string)content;
        Level level = JsonUtility.FromJson<Level>(json);
        level.Initialize();
        Debug.Log("json: " + json);
        level.Name = LevelCommon.NameWithoutPrefix(
            LevelCommon.NameWithoutExtension(name));
        return level;
    }
}
