﻿public abstract class LevelLoader {
    public Level Load(string name) {
        return LoadInternal(name);
    }

    public Level LoadContent(string name, object content) {
        return LoadContentInternal(name, content);
    }

    protected abstract Level LoadInternal(string fullName);
    protected abstract Level LoadContentInternal(string name, object content);
}
