﻿/**
* Gestión del movimiento de los elementos del nivel.
*/
using System.Collections.Generic;
using UnityEngine;

public class LevelMoveManager : LevelBehaviour {

    private Player playerRef;

    private Dictionary<LevelElement, IntVector2> originalPositions;
    private IntVector2 playerStartOriginalPosition;

    private void OnEnable() {
        ThisMaster.levelLoaded += OnLevelLoaded;
        ThisMaster.tryToMovePlayer += OnTryToMovePlayer;
        ThisMaster.levelElementSucessAdded += OnLevelElementSuccessAdded;
        ThisMaster.levelElementMoved += OnLevelElementMoved;
        ThisMaster.levelElementDeleted += OnLevelElementDeleted;
        ThisMaster.levelRestarted += OnLevelRestarted;
        ThisMaster.gameModeSetted += OnGameModeSetted;
    }

    private void OnDisable() {
        ThisMaster.levelLoaded -= OnLevelLoaded;
        ThisMaster.tryToMovePlayer -= OnTryToMovePlayer;
        ThisMaster.levelElementSucessAdded -= OnLevelElementSuccessAdded;
        ThisMaster.levelElementMoved -= OnLevelElementMoved;
        ThisMaster.levelElementDeleted -= OnLevelElementDeleted;
        ThisMaster.levelRestarted -= OnLevelRestarted;
        ThisMaster.gameModeSetted -= OnGameModeSetted;
    }

    private void OnLevelLoaded(Level level) {
        //this.level = level;
        originalPositions = new Dictionary<LevelElement, IntVector2>();
        playerRef = new Player(level.PlayerStart.Position);

        foreach(var element in level.Boxes) {
            originalPositions.Add(element, element.Position);
        }
        foreach (var element in level.Walls) {
            originalPositions.Add(element, element.Position);
        }
        foreach (var element in level.Goals) {
            originalPositions.Add(element, element.Position);
        }
        // originalPositions.Add(level.PlayerStart, level.PlayerStart.Position);
        playerStartOriginalPosition = level.PlayerStart.Position;
        // RestartPositions();
    }

    private void OnGameModeSetted(Master.GameMode gameMode) {
        // RestartPositions();
        /* switch(gameMode) {
            case Master.GameMode.testEditor:
                RestartPositions();
                break;
            case Master.GameMode.normalPlay:
                RestartPositions();
                break;
        }*/
    }

    private void OnTryToMovePlayer(IntVector2 direction) {
        Level level = ThisMaster.Level;
        IntVector2 newPosOfPlayer = new IntVector2(playerRef.Position.x +
            direction.x, playerRef.Position.y + direction.y);

        // Comprobar si hay obstáculos u objetos que se puedan mover en la
        // celda a la que se va a mover el jugador.
        List<LevelElement> targetCellContent = level.WhatsIn(newPosOfPlayer);
        bool isEmpty = IsEmpty(targetCellContent);
        LevelElement obstacle = GetObstacle(targetCellContent);
        LevelElement pushable = GetPushable(targetCellContent);
        
        if(isEmpty || obstacle == null) {
            MovePlayerTo(newPosOfPlayer);
        } else {
            if(pushable != null) {
                // Comprobar si en la celda a la que se va a mover el pushable
                // no hay ningún obstáculo.
                IntVector2 newPosOfPushable =
                    new IntVector2(newPosOfPlayer.x + direction.x,
                    newPosOfPlayer.y + direction.y);
                List<LevelElement> pushableTargetCellContent = 
                    level.WhatsIn(newPosOfPushable);
                bool targetOfPushableIsEmpty = 
                    IsEmpty(pushableTargetCellContent);
                LevelElement obstacleInPushableTarget = 
                    GetObstacle(pushableTargetCellContent);

                if(targetOfPushableIsEmpty ||
                    obstacleInPushableTarget == null) {
                    // Mover primero al jugador y luego la caja para que no
                    // ocurra primero el evento de goal por haber movido una
                    // caja a una goal.
                    MovePlayerTo(newPosOfPlayer);
                    MoveNonPlayerTo(pushable, newPosOfPushable);
                }
            }
        }
    }

    private bool IsEmpty(List<LevelElement> list) {
        return list.Count == 0;
    }

    private LevelElement GetObstacle(List<LevelElement> list) {
        return list.Find((e) => {
            return e.Obstacle;
        });
    }

    private LevelElement GetPushable(List<LevelElement> list) {
        return list.Find((e) => {
            return e.Pushable;
        });
    }

    private void MovePlayerTo(IntVector2 to) {
        playerRef.Position = new IntVector2(to.x, to.y);
        ThisMaster.CallPlayerMoved(to);
    }

    private void MoveNonPlayerTo(LevelElement element, IntVector2 to) {
        IntVector2 from = element.Position;
        ThisMaster.Level.MoveElement(element, to);
        ThisMaster.CallElementMoved(element, from, to);

        if(Master.EditorMode) {
            if(element.GetType() != typeof(PlayerStart)) {
                originalPositions[element] = to;
            } else {
                playerStartOriginalPosition = to;
                playerRef.Position = to;
            }
        }
    }

    private void OnLevelElementSuccessAdded(LevelElement element) {
        if (Master.EditorMode) {
            if(element.GetType() != typeof(PlayerStart)) {
                if (originalPositions.ContainsKey(element)) {
                    originalPositions[element] = element.Position;
                } else {
                    originalPositions.Add(element, element.Position);
                }
            } else {
                playerStartOriginalPosition = element.Position;
                playerRef.Position = element.Position;
            }
        }
    }

    // Es posible que los elementos sean movidos no por este script (cuando se 
    // un playerStart, que realmente lo que se hace es mover el ya existente).
    // TODO cambiar
    private void OnLevelElementMoved(LevelElement element, IntVector2 from,
        IntVector2 to) {
        if (Master.EditorMode) {
            if(element.GetType() != typeof(PlayerStart)) {
                originalPositions[element] = to;
            } /*else {
                playerStartOriginalPosition = to;
                playerRef.Position = to;
            }*/
        }
    }

    private void OnLevelElementDeleted(LevelElement element) {
        originalPositions.Remove(element);
    }

    private void OnLevelRestarted() {
        RestartPositions();
    }

    private void RestartPositions() {
        foreach(var element in originalPositions) {
            Debug.Log("restart 1");
            Debug.Log("move " + element.Key + " to " + element.Value);
            MoveNonPlayerTo(element.Key, element.Value);
            Debug.Log("restart 2");
        }
        MovePlayerTo(playerStartOriginalPosition);
    }
    
}
