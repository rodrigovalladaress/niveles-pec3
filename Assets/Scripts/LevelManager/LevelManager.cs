﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class LevelManager : LevelBehaviour {

    [SerializeField]
    private Master.GameMode initialMode = Master.GameMode.editor;
    [SerializeField]
    private TextAsset[] levels;
    [SerializeField]
    private int width = 40;
    [SerializeField]
    private int height = 20;

    private LevelLoader loader;
    private LevelWriter writer;

    private int currentLevelIndex = 0;

    private void OnEnable() {
        ThisMaster.levelFinished += OnLevelFinished;
        ThisMaster.openLevelRequested += OnOpenLevelRequested;
        ThisMaster.saveLevelRequested += OnSaveLevelRequested;
    }

    private void OnDisable() {
        ThisMaster.levelFinished -= OnLevelFinished;
        ThisMaster.openLevelRequested -= OnOpenLevelRequested;
        ThisMaster.saveLevelRequested -= OnSaveLevelRequested;
    }

    private new void Awake() {
        base.Awake();
        loader = new LevelLoaderJSON();
        writer = new LevelWriterJSON();
    }

    void Start () {
        Level level;

        if(levels == null || levels.Length == 0) {
            level = new Level(width, height);
        } else {
            TextAsset firstLevel = levels[0];
            level =
                loader.LoadContent(firstLevel.name, firstLevel.text);
        }
        ThisMaster.CallLevelLoaded(level);

        ThisMaster.CallGameModeSetted(initialMode);
	}

    private void WriteTest() {
        Debug.Log("write test");
        Level level = new Level(width, height);
        List<Box> boxes = new List<Box>();
        List<Goal> goals = new List<Goal>();
        boxes.Add(new Box(3, 4));
        boxes.Add(new Box(1, 2));
        goals.Add(new Goal(3, 5));
        level.Boxes = boxes;
        level.Goals = goals;
        level.PlayerStart = new PlayerStart(4, 4);
        writer.Write("prueba", level);
    }

    private void LoadTest() {
        Debug.Log("load test");
        Level level = loader.Load("prueba");
        Debug.Log(level.ToString());
        ThisMaster.CallLevelLoaded(level);
    }

    private void OnOpenLevelRequested() {
        Debug.Log("level requested");
        string path = EditorUtility.OpenFilePanel("Abrir un nivel", 
            LevelCommon.PREFIX, "json");
        if(path != null && path.Length != 0) {
            Debug.Log("path = " + path);
            LoadLevel(path);
        }
    }

    private void OnSaveLevelRequested() {
        var path = EditorUtility.SaveFilePanel(
                "Guardar nivel",
                LevelCommon.PREFIX,
                "*.json",
                "json");
        Debug.Log("path = " + path);
        if (path != null && path.Length != 0) {
            writer.Write(path, ThisMaster.Level);
            ThisMaster.CallSaveLevelSuccess(ThisMaster.Level);
        }
    }

    private void LoadLevel(string path) {
        Level level = loader.Load(path);
        ThisMaster.CallLevelLoaded(level);
        Debug.Log("loaded: " + level.ToString());
    }

    private void OnLevelFinished() {
        if(Master.NormalPlayMode && levels != null && levels.Length != 0) {
            NextLevel();
        }
    }

    private void NextLevel() {
        TextAsset levelContent;
        Level level;
        currentLevelIndex = (currentLevelIndex + 1) % levels.Length;
        levelContent = levels[currentLevelIndex];
        level = loader.LoadContent(levelContent.name, levelContent.text);
        ThisMaster.CallLevelLoaded(level);
        ThisMaster.CallLevelRestarted();
    }

}
