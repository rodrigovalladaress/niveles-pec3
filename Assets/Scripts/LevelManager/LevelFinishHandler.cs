﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelFinishHandler : LevelBehaviour{

    private int numGoals;
    private int goalsActive;

    private void OnEnable() {
        ThisMaster.levelLoaded += OnLevelLoaded;
        ThisMaster.gameModeSetted += OnGameModeSetted;
        ThisMaster.goalActivationDeltaChanged += OnGoalActivationDeltaChanged;
    }

    private void OnDisable() {
        ThisMaster.levelLoaded -= OnLevelLoaded;
        ThisMaster.gameModeSetted -= OnGameModeSetted;
        ThisMaster.goalActivationDeltaChanged -= OnGoalActivationDeltaChanged;
    }

    private void Initialize() {
        numGoals = ThisMaster.Level.Goals.Count;
        goalsActive = 0;
    }

    private void OnLevelLoaded(Level level) {
        Initialize();
    }

    private void OnGameModeSetted(Master.GameMode gameMode) {
        if(gameMode == Master.GameMode.normalPlay || 
            gameMode == Master.GameMode.testEditor) {
            Initialize();
        }
    }

    private void OnGoalActivationDeltaChanged(int delta) {
        goalsActive += delta;
        if(goalsActive == numGoals) {
            ThisMaster.CallLevelFinished();
        }
    }

}
