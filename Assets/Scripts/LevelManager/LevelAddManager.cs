﻿using UnityEngine;

public class LevelAddManager : LevelBehaviour {

    private void OnEnable() {
        ThisMaster.tryToAddElement += OnTryToAddElement;
    }

    private void OnDisable() {
        ThisMaster.tryToAddElement -= OnTryToAddElement;
    }

    private void OnTryToAddElement(LevelElement element) {
        IntVector2 from = ThisMaster.Level.PlayerStart.Position;
        IntVector2 to = element.Position;
        if(element.GetType() == typeof(PlayerStart)) {
            // Se debe eliminar antes el contenido de la celda.
            ThisMaster.CallLevelElementsDeletedIn(to);
            ThisMaster.Level.MoveElement(element, to);
            ThisMaster.CallElementMoved(element, from, to);
        } else {
            bool elementWasSuccessAdded = ThisMaster.Level.AddElement(element);
            if(!elementWasSuccessAdded) {
                // No se pudo añadir el elemento porque ya hay algo en esa 
                // celda.
                ThisMaster.CallLevelElementsDeletedIn(to);
                elementWasSuccessAdded = ThisMaster.Level.AddElement(element);
            }
            if(elementWasSuccessAdded) {
                ThisMaster.CallLevelElementSuccessAdded(element);
            }
        }
    }

}
