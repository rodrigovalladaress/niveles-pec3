﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDeleteManager : LevelBehaviour {

    private void OnEnable() {
        ThisMaster.levelElementsDeletedIn += OnLevelElementsDeletedIn;
    }

    private void OnDisable() {
        ThisMaster.levelElementsDeletedIn -= OnLevelElementsDeletedIn;
    }

    private void OnLevelElementsDeletedIn(IntVector2 position) {
        List<LevelElement> cellContent = ThisMaster.Level.WhatsIn(position);
        List<LevelElement> toDelete = new List<LevelElement>();

        // Se guardan las referencias a los elementos en otro array para poder
        // iterar por él mientras se eliminan los elementos.
        foreach (var e in cellContent) {
            Debug.Log("to delete " + e);
            toDelete.Add(e);
        }

        foreach (var e in toDelete) {
            // Llamar al evento levelElementDeleted antes de eliminar el propio
            // elemento para evitar referencias nulas.
            ThisMaster.CallLevelElementDeleted(e);
            ThisMaster.Level.DeleteElement(e);
        }
    }

}
