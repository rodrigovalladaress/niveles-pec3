﻿/**
 * Comprueba si cuando se mueve un box esta activa o desactiva un goal.
 */
using UnityEngine;

public class LevelGoalHandler : LevelBehaviour {

    private void OnEnable() {
        ThisMaster.levelElementMoved += OnLevelElementMoved;
    }

    private void OnDisable() {
        ThisMaster.levelElementMoved -= OnLevelElementMoved;
    }

    private void OnLevelElementMoved(LevelElement element, IntVector2 from,
        IntVector2 to) {
        if (Master.TestEditorMode || Master.NormalPlayMode) {
            if (element.GetType() == typeof(Box)) {
                Level level = ThisMaster.Level;

                bool theresGoalInOrigin = level.TheresAGoalIn(from);
                bool theresGoalInTarget = level.TheresAGoalIn(to);

                if(theresGoalInOrigin && !theresGoalInTarget) {
                    ThisMaster.CallGoalActivationDeltaChanged(-1);
                } else if(!theresGoalInOrigin && theresGoalInTarget) {
                    ThisMaster.CallGoalActivationDeltaChanged(1);
                }
            }
        }
    }

}
