﻿public static class LevelCommon {
    public const string PREFIX = "Assets/Levels/Resources";
    public const string SEPARATOR = "/";
    public const string EXTENSION = ".json";

    public static string PathWithPrefix(string path) {
        if(path.StartsWith(PREFIX)) {
            return path;
        } else {
            return PREFIX + SEPARATOR + path;
        }
    }

    public static string NameWithoutPrefix(string name) {
        int lastIndexOfSeparator = name.LastIndexOf(SEPARATOR);
        if(lastIndexOfSeparator == -1) {
            return name;
        } else {
            return name.Substring(lastIndexOfSeparator + 1);
        }
    }


    public static string NameWithExtension(string name) {
        if(name.EndsWith(EXTENSION)) {
            return name;
        } else {
            return name + EXTENSION;
        }
    }

    public static string NameWithoutExtension(string name) {
        if(!name.EndsWith(EXTENSION)) {
            return name;
        } else {
            return name.Substring(0, name.IndexOf(EXTENSION));
        }
    }

}
