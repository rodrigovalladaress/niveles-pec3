﻿/**
 * Dibuja los elementos del nivel (cajas, paredes, suelo, objetivos, jugador).
 * */
using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelDrawer : LevelBehaviour {

    [SerializeField]
    private GameObject boxPrefab;
    [SerializeField]
    private GameObject wallPrefab;
    [SerializeField]
    private GameObject goalPrefab;
    [SerializeField]
    private GameObject floorPrefab;
    [SerializeField]
    private GameObject playerPrefab;
    [SerializeField]
    private GameObject playerStartPrefab;

    [SerializeField]
    private Transform boxParent;
    [SerializeField]
    private Transform wallParent;
    [SerializeField]
    private Transform goalParent;
    [SerializeField]
    private Transform floorParent;
    [SerializeField]
    private Transform playerParent;
    [SerializeField]
    private Transform playerStartParent;

    [SerializeField]
    private RectTransform topPanel;

    private Player playerRef;
    private GameObject player;
    // private List<GameObject> boxes;
    private Dictionary<LevelElement, GameObject> levelElementToGameObject;
    private GameObject playerStartGameObject;

    private float floorMagnitude = 0f;
    private float halfFloorMagnitude = 0f;
    private Vector3 topLeft;
    private Vector3 topLeftElementPos;
    private Vector3 scale = Vector3.one;

    // Altura del panel superior de la GUI (para evitar que la GUI quede
    // encima del nivel que se va a editar.
    private float topPanelHeight;

    private new void Awake() {
        base.Awake();
        topPanelHeight = topPanel ? topPanel.rect.height / Screen.height : 0;
    }

    private void OnEnable() {
        ThisMaster.levelLoaded += OnLevelLoaded;
        ThisMaster.playerMoved += OnPlayerMoved;
        ThisMaster.levelElementMoved += OnLevelElementMoved;
        ThisMaster.levelElementSucessAdded += OnLevelElementSuccessAdded;
        ThisMaster.levelElementDeleted += OnLevelElementDeleted;
        ThisMaster.gameModeSetted += OnGameModeSetted;
    }

    private void OnDisable() {
        ThisMaster.levelLoaded -= OnLevelLoaded;
        ThisMaster.playerMoved -= OnPlayerMoved;
        ThisMaster.levelElementMoved -= OnLevelElementMoved;
        ThisMaster.levelElementSucessAdded -= OnLevelElementSuccessAdded;
        ThisMaster.levelElementDeleted -= OnLevelElementDeleted;
        ThisMaster.gameModeSetted -= OnGameModeSetted;
        //ThisMaster.levelRestarted -= OnLevelRestarted;
    }

    private void OnLevelLoaded(Level level) {
        CleanLevel();
        CalculateWorldPointsAndScale();
        DrawLevel(level);
    }

    private void OnGameModeSetted(Master.GameMode gameMode) {
        if(player != null) {
            switch (gameMode) {
                case Master.GameMode.editor:
                    player.SetActive(false);
                    break;
                case Master.GameMode.normalPlay:
                    player.SetActive(true);
                    Move(player, ThisMaster.Level.PlayerStart.Position);
                    break;
                case Master.GameMode.testEditor:
                    player.SetActive(true);
                    Move(player, ThisMaster.Level.PlayerStart.Position);
                    break;
            }
        }
    }

    private void CalculateWorldPointsAndScale() {
        Level level = ThisMaster.Level;

        SpriteRenderer floorRenderer = floorPrefab.GetComponent<SpriteRenderer>();
        Vector3 floorSize = floorRenderer.bounds.size;
        Vector3 topRight;
        //Vector3 topRightElementPos;
        Vector3 bottomLeft;
        //Vector3 bottomLeftElementPos;
        float maxLevelMagnitude;
        float neededWidth;
        float neededHeight;
        float minFloorMagnitude = 0f;
        float partScale = 1f;

        if (floorSize.x > floorMagnitude) {
            floorMagnitude = floorSize.x;
        }
        if (floorSize.y > floorMagnitude) {
            floorMagnitude = floorSize.y;
        }
        if (floorSize.z > floorMagnitude) {
            floorMagnitude = floorSize.z;
        }

        maxLevelMagnitude = level.Height;
        if (level.Width > maxLevelMagnitude) {
            maxLevelMagnitude = level.Width;
        }

        topLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 1 - topPanelHeight, 5));
        topRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 5));
        bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 5));
        neededWidth = Math.Abs(topRight.x - topLeft.x) / level.Width;
        neededHeight = Math.Abs(bottomLeft.y - topLeft.y) / level.Height;
        minFloorMagnitude = neededWidth < neededHeight ? neededWidth : neededHeight;
        partScale = minFloorMagnitude / floorMagnitude;
        scale = new Vector3(partScale, partScale, partScale);

        floorMagnitude = minFloorMagnitude;
        halfFloorMagnitude = floorMagnitude / 2f;

        topLeftElementPos = new Vector3(topLeft.x + halfFloorMagnitude,
            topLeft.y - halfFloorMagnitude, topLeft.z);
    }

    private void CleanLevel() {
        if(levelElementToGameObject != null) {
            foreach(var element in levelElementToGameObject) {
                Destroy(element.Value.gameObject);
            }
        }
        if(playerStartGameObject != null) {
            Destroy(playerStartGameObject);
        }
        if(player != null) {
            Destroy(player);
            player = null;
        }
    }

    private void DrawLevel(Level level) {
        levelElementToGameObject = new Dictionary<LevelElement, GameObject>();

        DrawFloor();
        foreach (var box in level.Boxes) {
            Draw(box);
        }
        foreach (var goal in level.Goals) {
            Draw(goal);
        }
        foreach (var wall in level.Walls) {
            Draw(wall);
        }
        DrawPlayerStart();

        playerRef = new Player(level.PlayerStart.Position);
        DrawPlayer();

        ThisMaster.CallLevelDrawn();
    }

    private void DrawFloor() {
        Level level = ThisMaster.Level;

        int width = level.Width;
        int height = level.Height;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                Draw(new Floor(i, j));
            }
        }
    }

    private GameObject DrawPlayerStart() {
        return Draw(ThisMaster.Level.PlayerStart);
    }

    private GameObject DrawPlayer() {
        
        player = Draw(playerRef);


        if (Master.NormalPlayMode || Master.TestEditorMode) {
            player.SetActive(true);
        } else {
            player.SetActive(false);
        }

        return player;
    }

    private GameObject Draw(LevelElement element) {
        return Draw(element, element.GetType());
    }

    private GameObject Draw(LevelElement element, Type type) {
        GameObject prefab = null;
        Transform parent = null;
        string name = "";

        if (type == typeof(Box)) {
            prefab = boxPrefab;
            parent = boxParent;
            name = "box";
        } else if (type == typeof(Wall)) {
            prefab = wallPrefab;
            parent = wallParent;
            name = "wall";
        } else if (type == typeof(Goal)) {
            prefab = goalPrefab;
            parent = goalParent;
            name = "goal";
        } else if (type == typeof(PlayerStart)) {
            prefab = playerStartPrefab;
            parent = playerStartParent;
            name = "playerStart";
        } else if (type == typeof(Player)) {
            prefab = playerPrefab;
            parent = playerParent;
            name = "playerVisual";
        } else if (type == typeof(Floor)) {
            prefab = floorPrefab;
            parent = floorParent;
            name = "floor";
        }
        name += " (" + element.Position.x + ", " + element.Position.y + ")";

        return Draw(element, prefab, parent, name);
    }

    private GameObject Draw(LevelElement element, GameObject prefab,
        Transform parent, string name) {
        Vector3 worldPoint = GridToWorldPoint(element.Position);

        GameObject o = Instantiate(prefab, worldPoint,
            Quaternion.identity, parent);
        o.name = name;
        o.transform.localScale = scale;

        ThisMaster.CallLevelElementDrawn(element, o);
        o.GetComponent<LevelElementInfo>().LevelElement = element;

        if(element.GetType() != typeof(Player)) {
            levelElementToGameObject.Add(element, o);
        }

        if (element.GetType() == typeof(PlayerStart)) {
            playerStartGameObject = o;
        }

        return o;
    }

    private Vector3 GridToWorldPoint(IntVector2 gridPosition) {
        int x = gridPosition.x;
        int y = gridPosition.y;
        float realX = topLeftElementPos.x + x * floorMagnitude;
        float realY = topLeftElementPos.y - y * floorMagnitude;
        float realZ = topLeftElementPos.z;
        return new Vector3(realX, realY, realZ);
    }

    private void OnPlayerMoved(IntVector2 to) {
        Move(player, to);
    }

    private void Move(GameObject o, IntVector2 to) {
        Vector3 worldPoint = GridToWorldPoint(to);

        o.transform.position = worldPoint;
    }

    private void OnLevelElementMoved(LevelElement element, IntVector2 from,
        IntVector2 to) {
        GameObject o;
        if (element.GetType() != typeof(PlayerStart)) {
            o = levelElementToGameObject[element];
        } else {
            o = playerStartGameObject;
            playerRef.Position = to;
            Move(player, to);
        }

        if(o != null) {
            // Debug.Log("level element moved: " + o.name);
            // o.transform.position = GridToWorldPoint(to);
            Move(o, to);
        }
    }

    private void OnLevelElementSuccessAdded(LevelElement element) {
        Draw(element);
    }

    private void OnLevelElementDeleted(LevelElement element) {
        Debug.Log(element + " deleted");
        if(element.GetType() != typeof(PlayerStart)) {
            GameObject o = levelElementToGameObject[element];
            Destroy(o);
        }
    }

}
