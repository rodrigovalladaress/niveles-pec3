﻿public abstract class LevelWriter {
    public void Write(string name, Level level) {
        level.Name = LevelCommon.NameWithoutPrefix(
            LevelCommon.NameWithoutExtension(name));
        WriteInternal(LevelCommon.NameWithExtension(name), level);
    }

    public void Write(string path, string name, Level level) {
        level.Name = LevelCommon.NameWithoutPrefix(
            LevelCommon.NameWithoutExtension(name));
        WriteInternal(LevelCommon.PathWithPrefix(path) +
            LevelCommon.SEPARATOR + LevelCommon.NameWithExtension(name),
            level);
    }

    protected abstract void WriteInternal(string fullName, Level level);
}
