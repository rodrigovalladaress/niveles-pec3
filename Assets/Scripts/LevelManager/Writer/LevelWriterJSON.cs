﻿using System.IO;
using UnityEngine;

public class LevelWriterJSON : LevelWriter {

    protected override void WriteInternal(string fullName, Level level) {
        Debug.Log("saving " + fullName);
        string json = JsonUtility.ToJson(level);
        Debug.Log("json level: " + json);
        File.WriteAllText(fullName, json);
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
    }
}
