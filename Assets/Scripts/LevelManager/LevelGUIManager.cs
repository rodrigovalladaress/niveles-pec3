﻿using UnityEngine;
using UnityEngine.UI;

public class LevelGUIManager : LevelBehaviour {

    [SerializeField]
    private Text levelNameGUI;
    [SerializeField]
    private KeyCode restartKeyCode = KeyCode.R;

    private void OnEnable() {
        ThisMaster.levelLoaded += OnLevelLoaded;
    }

    private void OnDisable() {
        ThisMaster.levelLoaded -= OnLevelLoaded;
    }

    private void OnLevelLoaded(Level level) {
        UpdateLevelName(level.Name);
    }

    private void UpdateLevelName(string name) {
        if(Master.NormalPlayMode || 
            Master.CurrentGameMode == Master.GameMode.none) {
            if(levelNameGUI != null) {
                levelNameGUI.text = name;
            }
        }
    }

    private void Update() {
        if(Master.NormalPlayMode && Input.GetKeyDown(restartKeyCode)) {
            ThisMaster.CallLevelRestarted();
        }
    }

}
