﻿using System.Collections;
using UnityEngine;

public class PlayerController : PlayerBehaviour {

    [SerializeField]
    // Segundos entre comprobaciones de inputs.
    private float secondsBetweenMoveChecks = 0.15f;

    private bool canMove = true;

    // ¿Esperando a que vuelvan a comprobarse los inputs?
    private bool notWaitingForNextMoveCheck = true;

    private void OnEnable() {
        ThisMaster.levelDrawn += OnLevelDrawn;
    }

    private void OnDisable() {
        ThisMaster.levelDrawn -= OnLevelDrawn;
    }

    private void OnLevelDrawn() {
        canMove = true;
    }

    private void Update () {
		if(canMove &&  notWaitingForNextMoveCheck &&
            (Master.NormalPlayMode || Master.TestEditorMode)) {
            int h = (int)Input.GetAxisRaw("Horizontal");
            int v = ((int)Input.GetAxisRaw("Vertical")) * -1;
            if(h != 0 || v != 0) {
                // Evitar movimiento diagonal.
                if(Mathf.Abs(h) == 1 && Mathf.Abs(v) == 1) {
                    v = 0;
                }
                ThisMaster.CallTryToMovePlayer(new IntVector2(h, v));
                StartCoroutine(WaitForNextMoveCheck());
                //canMove = false;
            }
        }
	}

    private IEnumerator WaitForNextMoveCheck() {
        notWaitingForNextMoveCheck = false;
        yield return new WaitForSeconds(secondsBetweenMoveChecks);
        notWaitingForNextMoveCheck = true;
    }
}
