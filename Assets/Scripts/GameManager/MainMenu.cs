﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    [SerializeField]
    private string storyScene;
    [SerializeField]
    private string editorScene;

    public void LoadStory() {
        SceneManager.LoadScene(storyScene);
    }

    public void LoadEditor() {
        SceneManager.LoadScene(editorScene);
    }

}
