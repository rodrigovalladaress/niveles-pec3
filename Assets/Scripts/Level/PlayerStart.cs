﻿using System;

[System.Serializable]
public class PlayerStart : LevelElement {

    public override bool Obstacle {
        get {
            return false;
        }
    }

    public override bool Pushable {
        get {
            return false;
        }
    }

    public PlayerStart(int x, int y) : base(x, y) {
    }

    public PlayerStart(IntVector2 position) : base(position) {
    }

    public override string ToString() {
        return "playerStart: " + base.ToString();
    }
}