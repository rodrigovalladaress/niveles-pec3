﻿public class Floor : LevelElement {

    public Floor(int x, int y) : base(x, y) {
    }

    public override bool Obstacle {
        get {
            return false;
        }
    }

    public override bool Pushable {
        get {
            return false;
        }
    }

    public override string ToString() {
        return "floor: " + base.ToString();
    }
}
