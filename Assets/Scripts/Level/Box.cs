﻿[System.Serializable]
public class Box : LevelElement {

    public override bool Obstacle {
        get {
            return true;
        }
    }

    public override bool Pushable {
        get {
            return true;
        }
    }

    public Box(int x, int y) : base(x, y) {
    }

    public Box(IntVector2 position) : base(position) {
    }

    public override string ToString() {
        return "box: " + base.ToString();
    }
}
