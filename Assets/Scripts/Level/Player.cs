﻿public class Player : LevelElement {

    public Player(int x, int y) : base(x, y) {
    }

    public override bool Obstacle {
        get {
            return false;
        }
    }

    public override bool Pushable {
        get {
            return false;
        }
    }

    public Player(IntVector2 position) : base(position) { }

    public override string ToString() {
        return "player: " + base.ToString();
    }
}
