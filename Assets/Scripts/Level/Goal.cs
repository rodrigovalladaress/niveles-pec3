﻿[System.Serializable]
public class Goal : LevelElement {
    public Goal(IntVector2 position) : base(position) {
    }

    public Goal(int x, int y) : base(x, y) {
    }

    public override bool Obstacle {
        get {
            return false;
        }
    }

    public override bool Pushable {
        get {
            return false;
        }
    }

    public override string ToString() {
        return "goal: " + base.ToString();
    }
}
