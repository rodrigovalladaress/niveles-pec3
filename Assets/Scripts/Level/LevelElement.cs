﻿using UnityEngine;

[System.Serializable]
public struct IntVector2 {
    public int x;
    public int y;

    public IntVector2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public override string ToString() {
        return "(" + x + ", " + y + ")";
    }

    public static bool operator ==(IntVector2 i1, IntVector2 i2) {
        return i1.x == i2.x && i1.y == i2.y;
    }

    public static bool operator !=(IntVector2 i1, IntVector2 i2) {
        return !(i1 == i2);
    }
}


[System.Serializable]
public abstract class LevelElement {

    public const int NOT_INITIALIZED = -9999;

    public abstract bool Obstacle {
        get;
    }

    public abstract bool Pushable {
        get;
    }

    [SerializeField]
    protected IntVector2 position;

    public LevelElement(IntVector2 position) {
        this.position = position;
    }

    public LevelElement(int x, int y) {
        position = new IntVector2(x, y);
    }

    public LevelElement() {
        position = new IntVector2(NOT_INITIALIZED, NOT_INITIALIZED);
    }

    public IntVector2 Position {
        get {
            return position;
        }

        set {
            position = value;
        }
    }

    public override string ToString() {
        return position.ToString();
    }

}
