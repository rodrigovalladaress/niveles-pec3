﻿using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Level {

    public const string DEFAULT_NAME = "untitled";

    [SerializeField]
    private int width = 40;
    [SerializeField]
    private int height = 20;

    [SerializeField]
    private List<Box> boxes;
    [SerializeField]
    private List<Wall> walls;
    [SerializeField]
    private List<Goal> goals;
    [SerializeField]
    private PlayerStart playerStart;

    // Posición de los elementos en tiempo de ejecución.
    private List<List<List<LevelElement>>> grid;

    private string name = DEFAULT_NAME;

    public Level(int width, int height) {
        this.width = width;
        this.height = height;
        boxes = new List<Box>();
        walls = new List<Wall>();
        goals = new List<Goal>();
        playerStart = new PlayerStart(0, 0);
        Initialize();
    }

    public void Initialize() {
        grid = new List<List<List<LevelElement>>>();
        for(int i = 0; i < width; i++) {
            grid.Add(new List<List<LevelElement>>());
            for(int j = 0; j < height; j++) {
                grid[i].Add(new List<LevelElement>());
            }
        }

        AddElements(boxes);
        AddElements(walls);
        AddElements(goals);
        AddElement(playerStart);
    }

    private void AddElements<T>(List<T> elementsToAdd) {
        if(elementsToAdd != null) {
            foreach(var element in elementsToAdd) {
                LevelElement levelElement = element as LevelElement;
                // Se pasa false para no añadir los elementos al array (ya
                // están en él).
                AddElement(levelElement, false);
            }
        }
    }

    // Los elementos que se añadan desde una clase fuera de Level siempre se
    // añadirán a los array del level.
    public bool AddElement(LevelElement elementToAdd) {
        return AddElement(elementToAdd, true);
    }

    private bool AddElement(LevelElement elementToAdd, bool addToArray) {
        bool elementWasAdded = false;

        if(elementToAdd != null) {
            IntVector2 position = elementToAdd.Position;

            List<LevelElement> cellContent = WhatsIn(position);
            bool cellEmpty = cellContent.Count == 0 &&
                playerStart.Position != position;

            if(cellEmpty) {
                grid[position.x][position.y].Add(elementToAdd);
                elementWasAdded = true;
                if(addToArray) {
                    AddToArray(elementToAdd);
                }
            } else {
                Debug.Log(elementToAdd.Position + " no está vacío: ");
                foreach(var e in cellContent) {
                    Debug.Log(e);
                }
            }
        }
        return elementWasAdded;
    }

    private void AddToArray(LevelElement elementToAdd) {
        Type typeofElementToAdd = elementToAdd.GetType();
        if(typeofElementToAdd == typeof(Box)) {
            boxes.Add((Box)elementToAdd);
        } else if(typeofElementToAdd == typeof(Wall)) {
            walls.Add((Wall)elementToAdd);
        } else if(typeofElementToAdd == typeof(Goal)) {
            goals.Add((Goal)elementToAdd);
        } else if(typeofElementToAdd == typeof(PlayerStart)) {
            if(playerStart == null) {
                playerStart = (PlayerStart)elementToAdd;
            } else {
                MoveElement(playerStart, elementToAdd.Position);
            }
        }
    }

    public void MoveElement(LevelElement element, IntVector2 to) {
        MoveElement(element, to.x, to.y);
    }

    public void MoveElement(LevelElement element, int x, int y) {
        if (x >= 0 && y >= 0 && x < width && y < height) {
            if(element.GetType() != typeof(PlayerStart)) {
                int prevX = element.Position.x;
                int prevY = element.Position.y;

                element.Position = new IntVector2(x, y);
                grid[prevX][prevY].Remove(element);
                grid[x][y].Add(element);
            } else {
                playerStart.Position = new IntVector2(x, y);
            }
        }
    }

    public void DeleteElement(LevelElement element) {
        Type type = element.GetType();
        IntVector2 position = element.Position;
        
        if(type == typeof(Box)) {
            boxes.Remove((Box)element);
        } else if(type == typeof(Wall)) {
            walls.Remove((Wall)element);
        } else if(type == typeof(Goal)) {
            goals.Remove((Goal)element);
        } else if(type == typeof(PlayerStart)) {
            Debug.LogError("No se puede eliminar playerStart");
        }
        if(type != typeof(PlayerStart)) {
            grid[position.x][position.y].Remove(element);
        }
    }

    public List<LevelElement> WhatsIn(IntVector2 position) {
        return WhatsIn(position.x, position.y);
    }

    public List<LevelElement> WhatsIn(int x, int y) {
        if(x >= 0 && y >= 0 && x < width && y < height) {
            return grid[x][y];
        } else {
            List<LevelElement> list = new List<LevelElement>();
            list.Add(new Wall(x, y));
            return list;
        }
    }

    public bool TheresAGoalIn(IntVector2 position) {
        return TheresAGoalIn(position.x, position.y);
    }

    public bool TheresAGoalIn(int x, int y) {
        List<LevelElement> elements = WhatsIn(x, y);
        return elements.Find((e) => {
            return e.GetType() == typeof(Goal);
        }) != null;
    }

    public List<Box> Boxes {
        get {
            return boxes;
        }

        set {
            boxes = value;
            AddElements(boxes);
        }
    }

    public List<Wall> Walls {
        get {
            return walls;
        }

        set {
            walls = value;
            AddElements(walls);
        }
    }

    public List<Goal> Goals {
        get {
            return goals;
        }

        set {
            goals = value;
            AddElements(goals);
        }
    }

    public PlayerStart PlayerStart {
        get {
            return playerStart;
        }

        set {
            playerStart = value;
            AddElement(playerStart);
        }
    }

    public int Height {
        get {
            return height;
        }

        set {
            height = value;
        }
    }

    public int Width {
        get {
            return width;
        }

        set {
            width = value;
        }
    }

    public string Name {
        get {
            return name;
        }

        set {
            name = value;
        }
    }

    public override string ToString() {
        string toString = "";

        toString += "{ width: " + width + ", height: " + height + ", boxes: " +
            arrayToString(boxes) + ", walls:" +
            arrayToString(walls) + ", goals: " + arrayToString(goals) + 
            ", playerStart: " + 
            (playerStart != null? playerStart.ToString() : "null") + " }";

        return toString;
    }

    private string arrayToString<T>(List<T> v) {
        string toString = "[";
        int count = v.Count;

        for(int i = 0; i < count; i++) {
            toString += v[i].ToString() + (i < count - 1 ? ", " : "");
        }

        toString += "]";

        return toString;
    }

}
