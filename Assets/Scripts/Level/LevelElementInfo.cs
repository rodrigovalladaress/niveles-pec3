﻿using UnityEngine;

public class LevelElementInfo : MonoBehaviour {

    // Se inicializa en LevelDrawer.
    private LevelElement levelElement;

    public LevelElement LevelElement {
        get {
            return levelElement;
        }

        set {
            levelElement = value;
        }
    }
    
}
