﻿[System.Serializable]
public class Wall : LevelElement {
    public Wall(IntVector2 position) : base(position) {
    }

    public Wall(int x, int y) : base(x, y) {
    }

    public override bool Obstacle {
        get {
            return true;
        }
    }

    public override bool Pushable {
        get {
            return false;
        }
    }

    public override string ToString() {
        return "wall: " + base.ToString();
    }
}
