Final project for "Modding y creación de niveles" (Modding and level creation) of the Master's Degree in Design and Development of Videogames (UOC).

The goal of the project was to develop a Sokoban clone and a level editor from scratch, and design 10 levels of rising difficulty.

![picture](sample.PNG)

### Editor controls ###
* Tool selection:
	* Q: box
	* W: wall
	* E: wall
	* R: start point
* Left mouse button: draw
* Right mouse button: clear
* P: test the level
* S: save
* K: open

### Game controls ###
* Arrow keys: movement